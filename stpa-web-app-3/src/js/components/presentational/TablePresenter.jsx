import React from "react";
import PropTypes from "prop-types";

// const TablePresenter = ({ label, text, type, id, value, handleChange }) => (
//   <div className="form-group">
//     <label htmlFor={label}>{text}</label>
//     <input
//       type={type}
//       className="form-control"
//       id={id}
//       value={value}
//       onChange={handleChange}
//       required
//     />
//   </div>
// );
// TablePresenter.propTypes = {
//   label: PropTypes.string.isRequired,
//   text: PropTypes.string.isRequired,
//   type: PropTypes.string.isRequired,
//   id: PropTypes.string.isRequired,
//   value: PropTypes.string.isRequired,
//   handleChange: PropTypes.func.isRequired
// };
//


export class TablePresenter extends React.Component {

  renderTableHeader() {
    let header = Object.keys(this.props.Control_Actions[0]).slice(1);
    return header.map((key, index) => {return <th key={index}>{key}</th>})
  }

  renderTableData() {
    return this.props.Control_Actions.map((Control_Action) => {
      return (
        <tr key={Control_Action["Code"]} valign={"top"}>
          <td valign="middle" onDoubleClick={this.props.onDouble} onBlur={this.props.onLeave} contentEditable="false" suppressContentEditableWarning={true}>{Control_Action["Control action"]}</td>
          <td>
            <dl>
              {Control_Action["Not providing"].map(
                (UCA) => {return(
                  <div key={UCA["Reference"]}>
                    <dt>{UCA["Reference"]}</dt>
                    <dd onDoubleClick={this.props.onDouble} onBlur={this.props.onChangedUCA} contentEditable="false" suppressContentEditableWarning={true}> {UCA["UCA_Text"]} </dd>
                  </div>
                )}
              )}
            </dl>
            <input type="text" placeholder="Enter a new UCA here" onBlur={this.props.onNewUCA} />
          </td>
          <td>
            <dl>
              {Control_Action["Providing"].map(
                (UCA) => {return(
                  <div key={UCA["Reference"]}>
                    <dt>{UCA["Reference"]}</dt>
                    <dd onDoubleClick={this.props.onDouble} onBlur={this.props.onChangedUCA} contentEditable="false" suppressContentEditableWarning={true}> {UCA["UCA_Text"]} </dd>
                  </div>
                )}
              )}
            </dl>
            <input type="text" placeholder="Enter a new UCA here" onBlur={this.props.onNewUCA} />
          </td>
          <td>
            <dl>
              {Control_Action["Too early, late or out of order"].map(
                (UCA) => {return(
                  <div key={UCA["Reference"]}>
                    <dt>{UCA["Reference"]}</dt>
                    <dd onDoubleClick={this.props.onDouble} onBlur={this.props.onChangedUCA} contentEditable="false" suppressContentEditableWarning={true}> {UCA["UCA_Text"]} </dd>
                  </div>
                )}
              )}
            </dl>
            <input type="text" placeholder="Enter a new UCA here" onBlur={this.props.onNewUCA} />
          </td>
          <td>
            <dl>
              {Control_Action["Too long or short"].map(
                (UCA) => {return(
                  <div key={UCA["Reference"]}>
                    <dt>{UCA["Reference"]}</dt>
                    <dd onDoubleClick={this.props.onDouble} onBlur={this.props.onChangedUCA} contentEditable="false" suppressContentEditableWarning={true}> {UCA["UCA_Text"]} </dd>
                  </div>
                )}
              )}
            </dl>
            <input type="text" placeholder="Enter a new UCA here" onBlur={this.props.onNewUCA} />
          </td>
        </tr>
      )
    })
  }

  render() {
    return (
      <div>
        <button onClick={this.props.onHomeButtonClick}> Home Page </button>
        <table id="UCAs" width="1000" border="1">
          <colgroup>
            <col style={{width: "12%"}}/>
            <col style={{width: "22%"}}/>
            <col style={{width: "22%"}}/>
            <col style={{width: "22%"}}/>
            <col style={{width: "22%"}}/>
          </colgroup>
          <tbody>
            <tr>{this.renderTableHeader()}</tr>
            {this.renderTableData()}
          </tbody>
        </table>
      </div>
    );
  }

}

export default TablePresenter;
