import React, { Component } from "react";
import ReactDOM from "react-dom";
import Input from "../presentational/Input.jsx";
import TablePresenter from "../presentational/TablePresenter.jsx";
import HomePage from "../presentational/HomePage.jsx";
import control_diagram from "../../../../images/level-2-control-diagram.png"
import {plugins, clone, commit, push} from 'isomorphic-git';
import * as git from 'isomorphic-git';
import FS from "@isomorphic-git/lightning-fs";

class TableContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Control_Actions: [
        {
          "Code": "PSI",
          "Control action": "Send images to model",
          "Not providing": [
            {Reference: "UCA-PSI-1", UCA_Text: "Python script does not provide images when there are new images available"}
          ],
          "Providing": [
            {Reference: "UCA-PSI-100", UCA_Text: "Python script provides images which are old (> x seconds)"},
            {Reference: "UCA-PSI-101", UCA_Text: "Python script provides images of poor quality (blurry, dark etc)"}
          ],
          "Too early, late or out of order": [
            {Reference: "UCA-PSI-1000", UCA_Text: "Python script provides images too late (> x seconds) after environment images have been received from GStreamer"}
          ],
          "Too long or short": []
        }, {
          "Code": "GG",
          "Control action": "Send Go command to GStreamer",
          "Not providing": [
            {Reference: "UCA-GG-1", UCA_Text: "Python script does not provide a go cmd to start GStreamer when it is not running"},
            {Reference: "UCA-GG-2", UCA_Text: "Python script does not provide a go cmd with a valid config when GStreamer's config is inaccurate"}
          ],
          "Providing": [
            {Reference: "UCA-GG-100", UCA_Text: "Python script provides a go cmd with inaccurate config file to GStreamer"},
            {Reference: "UCA-GG-101", UCA_Text: "Python script provides a go cmd when GStreamer is already running with a valid config"}
          ],
          "Too early, late or out of order": [
            {Reference: "UCA-GG-1000", UCA_Text: "Python script provides a go cmd to GStreamer too late for the camera to send environment images within X seconds of systemd starting the Python script"},
            {Reference: "UCA-GG-1001", UCA_Text: "Python script provides a go cmd to GStreamer too late (> x seconds) after GStreamer has been inactive, or it is active with the wrong config"}
          ],
          "Too long or short": []
        }
      ],
      Page_to_Show: "Home Page",
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleNewUCA = this.handleNewUCA.bind(this);
    this.handleChangedUCA = this.handleChangedUCA.bind(this);
    this.handleDouble = this.handleDouble.bind(this);
    this.handleLeave = this.handleLeave.bind(this);
    this.handleUCAButton = this.handleUCAButton.bind(this);
    this.handleHomeButton = this.handleHomeButton.bind(this);
  }

  handleHomeButton(event) {
    this.setState({ Page_to_Show: "Home Page" });
  }

  handleUCAButton(event) {
    this.setState({ Page_to_Show: "UCA Page" });
  }

  tryCategory(obj, category) {
    if (obj[category]) {
      try {
        return obj[category].map((uca) => { return({
          Reference: uca["Reference"] || "",
          UCA_Text: uca["Text"] || ""
        })});
      } catch (TypeError) {return []}
    } else {
      return [];
    }
  }

  getControlActionList(yaml_file) {
    return yaml_file.map((ca) => {
      try {
        return (
          {
            "Code": ca["UCA"]["Code"] || "",
            "Control action": ca["UCA"]["Control action"] || "" ,
            "Not providing": this.tryCategory(ca["UCA"], "Not providing"),
            "Providing": this.tryCategory(ca["UCA"], "Providing"),
            "Too early, late or out of order": this.tryCategory(ca["UCA"], "Too early, late or out of order"),
            "Too long or short": this.tryCategory(ca["UCA"], "Too long or short"),
          }
        )
      } catch (TypeError) {return ({
        "Code": "",
        "Control action": "",
        "Not providing": [],
        "Providing": [],
        "Too early, late or out of order": [],
        "Too long or short": [],
      })}
    })
  }

  getColumn(ind) {
    return ["Not providing", "Providing", "Too early, late or out of order", "Too long or short"][ind - 1];
  }

  generateUCARef(column_num, row_num) {
    let category = this.state.Control_Actions[row_num-1][this.getColumn(column_num)];
    if (category.length > 0){
      let ref = category[category.length-1]["Reference"].split("-");
      ref[ref.length - 1] = (parseInt(ref[ref.length-1], 10)+1).toString();
      return ref.join("-");
    } else {
      return `UCA-${this.state.Control_Actions[row_num-1]["Code"]}-${10**(column_num).toString()}`;

    }
  }

  addUCA(current_CAs, newUCA, column_num, row_num) {
    let col = this.getColumn(column_num);
    current_CAs[row_num-1][col].push({
      Reference: this.generateUCARef(column_num, row_num),
      UCA_Text: newUCA
    })
    return current_CAs;
  }

  handleNewUCA(event) {
    if (event.target.value != "" && event.target.value != "Enter a new UCA here") {
      const value = event.target.value;
      const column_num = event.target.parentElement.cellIndex;
      const row_num = event.target.parentElement.parentElement.rowIndex;
      this.setState(prevState => ({
        Control_Actions: this.addUCA(prevState.Control_Actions, value, column_num, row_num)
      }));
      event.target.value = "";
    }
  }

  changeUCA(current_CAs, newUCA, column_num, row_num, reference) {
    const col = this.getColumn(column_num);
    const pos = current_CAs[row_num-1][col].findIndex((UCA)=>{
      return UCA["Reference"] === reference;
    })
    current_CAs[row_num-1][col][pos]["UCA_Text"] = newUCA;
    return current_CAs;
  }

  removeUCA(current_CAs, column_num, row_num, reference) {
    const col = this.getColumn(column_num);
    const pos = current_CAs[row_num-1][col].findIndex((UCA)=>{
      return UCA["Reference"] === reference;
    })
    current_CAs[row_num-1][col].splice(pos,1);
    return current_CAs;
  }

  handleChangedUCA(event) {
    if (event.target.textContent.length === 0) {
      const column_num = event.target.parentElement.parentElement.parentElement.cellIndex;
      const row_num = event.target.parentElement.parentElement.parentElement.parentElement.rowIndex;
      const ref = event.target.parentElement.firstElementChild.textContent;
      this.setState(prevState => ({
        Control_Actions: this.removeUCA(prevState.Control_Actions, column_num, row_num, ref)
      }));
    } else if (event.target.textContent != this.state.Last_UCA_Edited) {
      const value = event.target.textContent;
      const column_num = event.target.parentElement.parentElement.parentElement.cellIndex;
      const row_num = event.target.parentElement.parentElement.parentElement.parentElement.rowIndex;
      const ref = event.target.parentElement.firstElementChild.textContent;
      this.setState(prevState => ({
        Control_Actions: this.changeUCA(prevState.Control_Actions, value, column_num, row_num, ref)
      }));
      event.target.contentEditable = "false";
    } else {
      event.target.contentEditable = "false";
    }
  }

  handleChange(event) {
    this.setState({ [event.target.id]: event.target.value });
  }

  handleDouble(event) {
    event.target.contentEditable = "true";
    this.setState({Last_UCA_Edited: event.target.textContent});
  }

  changeCA(current_CAs, newCA, row_num) {
    if (newCA != "") {
      current_CAs[row_num-1]["Control action"] = newCA;
    }
    return current_CAs;
  }

  handleLeave(event) {
    event.target.contentEditable = "false";
    const row_num = event.target.parentElement.rowIndex;
    const value = event.target.textContent;
    if (value.length === 0) {
      event.target.textContent = this.state.Last_UCA_Edited;
    } else {
      this.setState(prevState => ({
        Control_Actions: this.changeCA(prevState.Control_Actions, value, row_num)
      }))
    };
  }

  render() {
    if (this.state.Page_to_Show === "UCA Page") {
      return(
        <TablePresenter Control_Actions={this.state.Control_Actions} onHomeButtonClick={this.handleHomeButton} onDefaultChange={this.handleChange} onLeave={this.handleLeave} onDouble={this.handleDouble} onNewUCA={this.handleNewUCA} onChangedUCA={this.handleChangedUCA}/>
      )
    } else {
      // const image_dir = `${this.state.dir}/images/level-2-control-diagram.png`;
      // console.log("IMAGE DIR!");
      // console.log(image_dir);
      // const control_diagram = images.level-2-control-diagram.png
      return(
        <div width="100%">
          <HomePage onButtonClick={this.handleUCAButton} />
        </div>
      )
    }

  }

  async componentDidMount () {
    const yaml = require("js-yaml");

    // const fs = require("fs");
    // window.fs = new FS("fs", {wipe:true});
    window.fs = new FS("fs");
    git.plugins.set("fs", window.fs);
    window.pfs = window.fs.promises;

    fs.rmdir("/",()=>{console.log(2)});
    fs.rmdir("/dirk",()=>{console.log(2)});
    fs.rmdir("/dirk2",()=>{console.log(2)});
    fs.rmdir("/dirk3",()=>{console.log(2)});
    fs.rmdir("/dirk4",()=>{console.log(2)});
    fs.rmdir("/dirk5",()=>{console.log(2)});
    fs.rmdir("/ddddd",()=>{console.log(2)});

    window.dir = "/ddddd";
    // window.dir = "/dirk";
    // window.dir = fs.mkdtemp(path.join(os.tmpdir(), "myTmpDir"));
    // window.dir = pfs.mkdtemp(path.join(os.tmpdir(), "myTmpDir"));
    // window.dir = fs.mkdir("/dirk", (fs)=>{console.log(fs);});
    // window.dir = fs.mkdir("dirk", (fs)=>{console.log(fs)});



    // window.dir = "/fabricated";
    // const nfs = require("fs");
    console.log("dir");
    console.log(dir);
    // window.gitdir = path.join(dir, '.git');
    // console.log("gitdir");
    // console.log(gitdir);
    // console.log(fs.read(`${gitdir}/config`, { encoding: 'utf8' }));
    // console.log("dir:");
    // console.log(dir);
    // window.pfs.mkdir(dir);

    // await pfs.mkdir(dir);
    // await pfs.readdir(dir);
    pfs.mkdir(dir);
    pfs.readdir(dir);
    // pfs.mkdir("/dirk");
    // pfs.readdir("/dirk");


    //console.log("pfs ");
    //console.log(window.pfs.readdir(dir));
    //console.log("gitclone me");
    //await console.log(git.clone({

    // await git.clone({
    git.clone({

      fs,
      dir,
      url: "https://cors-anywhere.herokuapp.com/https://gitlab.com/shaunmooneyct/av-stpa",
      // // dir:"/dirk",
      // // corsProxy: "http://127.00.00.1:5000/",
      // // corsProxy: 'https://cors.isomorphic-git.org',
      // url: "https://cors-anywhere.herokuapp.com/https://gitlab.com/milan.lakhani/crash-system-safety-analysis",
      // // url: "https://cors-anywhere.herokuapp.com/https://gitlab.com/celduin/crash/crash-system-safety-analysis:443",
      // // url: "http://127.00.00.1:5000/https://gitlab.com/celduin/crash/crash-system-safety-analysis",
      // // url: "http://127.00.00.1:5000/https://gitlab.com/celduin/crash/crash-system-safety-analysis:443",
      // // url: "https://gitlab.com/celduin/crash/crash-system-safety-analysis",
      ref: "master",
      // singleBranch: false,
      singleBranch: true,
      depth: 10
    });
    // All of the below commands should be await (whether assigned to vars or not)
    // pfs.readdir("/dirk");


    console.log("The dir")
    console.log(pfs.readdir(dir));
    console.log("gitlog1");
    console.log(git.log({dir}));

    console.log(fs);
    console.log(dir);

    console.log("status")
    console.log(git.status({dir, filepath:"README.md"}));

    const read_me = pfs.readFile(`${dir}/README.md`, 'utf8');
    console.log("README");
    console.log(read_me);

    console.log("Image string then image non string")
    const img_string = pfs.readFile(`${dir}/images/level-2-control-diagram.png`, 'utf8');
    const img_non_string = pfs.readFile(`${dir}/images/level-2-control-diagram.png`);
    console.log(img_string);
    console.log(img_non_string);


    console.log("THE FILE");
    // console.log(pfs.readFile(`${dir}/STPA/level-2/level-2-UCAs.yml`, "utf8"));

    // console.log("yaml");
    const string_doc = pfs.readFile(`${dir}/STPA/level-2/level-2-UCAs.yml`, 'utf8');
    console.log(string_doc);
    console.log(typeof string_doc);
    console.log(Object.values(string_doc));
    console.log("String from object");
    // const string_for_dict = string_doc.then((res)=>{
    //   console.log("res");
    //   console.log(res);
    //   return res;})
    string_doc.then((res)=>{
      console.log("res");
      console.log(res);
      // this.setState({yaml_string: res});
      // console.log("ystring");
      // console.log(this.state.yaml_string);
      return(res);
    }).then((res2)=>{
      const response = yaml.safeLoad(res2);
      console.log("Response");
      console.log(response);
      // this.setState({yaml_file: response});
      // console.log(this.state.yaml_file[0]["UCA"]["Controller"]);
      console.log(response[0]["UCA"]["Controller"]);
      console.log("SFF1");
      let stuff_from_file = this.getControlActionList(response);
      console.log("SFF2");
      console.log(stuff_from_file);
      this.setState({Control_Actions: stuff_from_file});
      // this.setState({Control_Actions: this.getControlActionList(response)});
    });


    // console.log("String from object 2");
    // // console.log(string_for_dict);
    // console.log(this.state.yaml_string);
    // // this.setState({yaml_string: string_for_dict});
    //
    // const response = yaml.safeLoad(this.state.yaml_string);
    // console.log("Response");
    // console.log(response);
    // this.setState({yaml_file: response});

  }

}
export default TableContainer;

const wrapper = document.getElementById("create-article-form");
wrapper ? ReactDOM.render(<TableContainer />, wrapper) : false;
