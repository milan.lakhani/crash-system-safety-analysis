import React, { Component } from 'react';
import ReactDOM from "react-dom";
import GitPresenter from "../presentational/GitPresenter.jsx";

class Gitter extends Component {
  constructor(props) {
    super(props);
    this.getUser = this.getUser.bind(this);
    this.state = {user:{}}
  }

  getUser () {
    const name = this.refs.name.value;
    fetch(`http://api.github.com/users/${name}`)
      .then(response => response.json())
      .then(data => {
        this.setState({
          user: {
            name: data.name,
            location: data.location
          }
        })
      })
  }
  // Fetch returns promise wrapped up in promise, so 2 thens

  render() {
    const { user } = this.state;
    return (
      <div className="App">
        <input type="text" placeholder="Enter a username" ref="name"/>
        <button onClick={this.getUser}>Get User Details</button>
        <GitPresenter user={user}/>
      </div>
    );
  }
}

export default Gitter;

const wrapper = document.getElementById("gittest");
wrapper ? ReactDOM.render(<Gitter />, wrapper) : false;
